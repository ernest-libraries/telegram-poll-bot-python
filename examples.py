import datetime
import json

import requests


TOKEN = '1447439986:AAGjFs1z7su9skUsLS4cYdyvEYIEkFA1zYo'
CHANNEL_ID = '-1001474915252'
CHANNEL_USERNAME = '@CFVID20_voting'
BASE_URL = 'https://api.telegram.org'
TRADING_CALENDAR = ['2020-12-30']
# '2020年12月22日（二）'
# https://www.hkex.com.hk/News/HKEX-Calendar?sc_lang=en


class CFVID20PollBot:
    def __init__(self, token, channel_id, channel_username):
        self.__base_url = 'https://api.telegram.org'
        self.__bot_token = token
        self.__channel_id = channel_id
        self.__channel_username = channel_username

        self.__message_id = None
        self.__start_poll_details = None
        self.__stop_poll_details = None
        self.__poll_id = None

        self.__poll_running = False
        self.__poll_just_had_run = False

    def __clean_and_initialize_message_id(self):
        self.__message_id = None

    def __clean_and_initialize_start_poll_details(self):
        self.__start_poll_details = None

    def __clean_and_initialize_stop_poll_details(self):
        self.__stop_poll_details = None

    def __clean_and_initialize_poll_id(self):
        self.__poll_id = None

    # TODO: Fix voting_date_for_dict
    def __fetch_start_poll_params(self, voting_date_for_dict={'year': 2021, 'month': 12, 'day': 22}):
        voting_date_for = f'/{voting_date_for_dict["year"]}-{voting_date_for_dict["month"]}-{voting_date_for_dict["day"]}'
        voting_dt = datetime.datetime.strptime(date_string=voting_date_for, format='%Y-%m-%d')
        return {
            'chat_id': self.__channel_username,
            'question': f'【 大戶指數 CFVID-20 】\n '
                        f'{voting_date_for} ({voting_dt.strftime("%A")[0:3]}) 投升跌',
            'options': ['升📈', '跌📉']
        }

    def start_poll(self):
        self.__clean_and_initialize_start_poll_details()

        params = self.__fetch_start_poll_params()
        response = requests.post(f'{self.__base_url}/bot{self.__bot_token}/sendPoll', json=params)

        self.__check_status_code_and_log(status_code=response.status_code)

        self.__start_poll_details = json.loads(response.text)
        if self.__start_poll_details['ok']:
            self.__clean_and_initialize_message_id()
            self.__message_id = self.__start_poll_details['result']['message_id']
        else:
            print('')

    def __fetch_stop_poll_params(self, message_id):
        return {
            'chat_id': self.__channel_username,
            'message_id': message_id
        }

    def stop_poll(self, message_id):
        self.__clean_and_initialize_stop_poll_details()

        params = self.__fetch_stop_poll_params(message_id=message_id)
        response = requests.post(f'{self.__base_url}/bot{self.__bot_token}/stopPoll', data=params)

        self.__check_status_code_and_log(status_code=response.status_code)

        self.__stop_poll_details = json.loads(response.text)
        if self.__stop_poll_details['ok']:
            self.__clean_and_initialize_poll_id()
            self.__poll_id = self.__stop_poll_details['result']['id']
        else:
            print('')

    def delete_poll_message(self, message_id):
        params = {
            'chat_id': self.__channel_username,
            'message_id': message_id
        }
        response = requests.post(f'{self.__base_url}/bot{self.__bot_token}/deleteMessage', data=params)

        self.__check_status_code_and_log(status_code=response.status_code)
        print(f'xxxx is deleted.')

    def record_poll_details(self):
        pass

    def review_poll_performance(self):
        pass

    def fetch_poll_performance_log(self):
        pass

    def send_performance_message(self):
        pass

    def run(self):
        pass
        now = datetime.datetime.now()
        if self.__poll_running and now.time() >= datetime.time(hour=9, minute=14, second=55, microsecond=0):
            self.stop_poll(message_id=self.__message_id)
            pass

        if self.__poll_running and now.time() >= datetime.time(hour=16, minute=35, second=0, microsecond=0):
            # self.review_poll_performance()
            pass

    def terminate(self):
        pass

    def check_bot_status(self):
        pass

    def check_process_health(self):
        pass

    @staticmethod
    def __check_status_code_and_log(status_code):
        if status_code == 200:
            print('nice')
        else:
            print('')
