import configparser
from pathlib import Path

import requests

# def get_project_path() -> Path:
#     return Path(__file__).parent.parent

PROJECT_PATH = Path(__file__).parent.parent
CONFIG_PATH = f'{PROJECT_PATH}/configs/config.ini'
config = configparser.ConfigParser()
config.read(CONFIG_PATH)

tg_bot_token = str(config.get('CREDENTIALS', 'TELEGRAM_BOT_TOKEN'))
cid = str(config.get('CREDENTIALS', 'CHANNEL_ID'))


